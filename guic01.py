#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gtk, GObject
import serial.tools.list_ports
import serial, thread
import psycopg2
# load the psycopg extras module
import psycopg2.extras
#import RPi.GPIO as GPIO
#import MFRC522

import json, requests, time
from  requests.exceptions import *

serverAddress = "http://localhost"
serverPort = "8000"
apiu = "/evento/api/registro/1.2v001/update"

nameDB = "nameDB"
userDB = "postgres"
hostDB = "localhost"
passwordDB = "password"

class myBuilder:
    """docstring for myBuilder."""
    def __init__(self,):
        try:
            self.conn = psycopg2.connect(dbname=nameDB,user=userDB,host=hostDB,password=passwordDB)
        except:
            print "No se puede conectar"
        self.ser = None
        self.builder = Gtk.Builder()
        self.builder.add_from_file("eje01.glade")
        self.handlers = {
			"onDeleteWindow": self.onDeleteWindow,
			"onClick": self.onClick,
			"onTree": self.onTree,
            "onActivate": self.onActivate,
            "onEdited":self.onEdited,
            "onPortConect":self.onPortConect,
            "onUnselect":self.onUnselect}
        self.builder.connect_signals(self.handlers)
        self.software_liststore = self.builder.get_object("liststore1")
        self.status = self.builder.get_object('status')
        self.dialog = self.builder.get_object("message")
        self.listports = self.builder.get_object('listport')
        self.dialog1 = self.builder.get_object('dialog1')


        self.name_store = Gtk.ListStore(int, str)

        self.llenarPuertos()

        self.tree = self.builder.get_object("treeview1")
        self.window = self.builder.get_object("window1")
        #self.window.fullscreen()
        self.window.show_all()


    def llenarPuertos(self):
        pass

    def llenarLista(self):
        pass

    def onPortConect(self,combo):
        pass

    def onClick(self, widget):
        pass

    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)

    def onTree(self,user):
        pass

    def onActivate(self,entry):
        pass

    def onUnselect(self,widget,text):
        pass

    def onEdited(self,widget,path,nText):
        pass


if __name__ == "__main__":
    GObject.threads_init()
    main = myBuilder()
    Gtk.main()
