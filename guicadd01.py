#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import serial
import serial.tools.list_ports
import json, requests
from  requests.exceptions import *


serverAddress = "http://localhost"
serverPort = "8000"
apiu = "/evento/api/add/1.2v001/"
api = "/evento/api/boletos/1.2v001/get/"

class RegistroAdd:
	builder=None
	def __init__(self):
		# Iniciamos el GtkBuilder para tirar del fichero de glade
		self.ser = None
		self.builder = Gtk.Builder()
		self.builder.add_from_file("ventanas.glade")
		self.handlers = {
			"onDeleteWindow": self.onDeleteWindow,
			"onPortConect":self.onPortConect,
			"onClick":self.onClick,
			"onRegistrar":self.onRegistrar,
			"onGenero":self.onGenero}

		self.impriFlag = False
		#**********************************************************************
		self.builder.connect_signals(self.handlers)
		self.window = self.builder.get_object("window1")
		self.correoForm = self.builder.get_object("correoForm")
		self.nombreForm = self.builder.get_object("nombreForm")
		self.apellidosForm = self.builder.get_object("apellidosForm")
		self.folioForm = self.builder.get_object("folioForm")
		self.uidForm = self.builder.get_object("uidForm")
		self.boletoForm = self.builder.get_object("boletoForm")
		self.generoForm = self.builder.get_object("generoForm")
		self.btnRegistrar = self.builder.get_object("btnRegistrar")
		self.btnImprimir = self.builder.get_object("btnImprimir")
		self.telefonoForm = self.builder.get_object("telefonoForm")
		self.empresaForm = self.builder.get_object("empresaForm")
		self.status = self.builder.get_object("status")
		self.alertaForm = self.builder.get_object("alertaForm")
		self.impresora = self.builder.get_object("impresora")
		self.dropForm = self.builder.get_object("dropForm")
		self.dialog = self.builder.get_object("message")
		#**********************************************************************

		self.llenarPuertos()
		self.llenarLista()


		self.window.show_all()
		self.window.resize(900,300)

	def onGenero(self,combo):
		pass

	def llenarLista(self):
		pass

	def onDeleteWindow(self, *args):
		Gtk.main_quit(*args)

	def onPortConect(self,combo):
		pass

	def llenarPuertos(self):
		pass

	def onClick(self, widget):
		pass

	def onRegistrar(self,widget):
		pass


if __name__ == '__main__':
	window = RegistroAdd()
	Gtk.main()
